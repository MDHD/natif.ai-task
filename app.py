from fastapi import FastAPI, Body, HTTPException, status
from fastapi.responses import Response, JSONResponse
from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel, Field
from typing import Optional, List
import motor.motor_asyncio
from uuid import UUID, uuid4
from enum import Enum

app = FastAPI()
client = motor.motor_asyncio.AsyncIOMotorClient("mongodb+srv://<username>:<yourPassword>@cluster0.iawtt.mongodb.net/?retryWrites=true&w=majority")
db = client.inteviewTask



class TVShow(str, Enum):
    breaking_bad = "breaking_bad"
    the_wire = "the_wire"


class UserModel(BaseModel):
    #default key for the mongoDB id is "_id" 
    id: UUID = Field(default_factory=uuid4, alias="_id")
    name: str
    favorite_tv_show: TVShow

    class Config:
        allow_population_by_field_name = True #in order to set the _id
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "name": "Mohamad Hammoud",
                "favorite_tv_show": "breaking_bad"
            }
        }


class UpdateUserModel(BaseModel):
    name: Optional[str]
    favorite_tv_show: Optional[TVShow]

    class Config:
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "name": "Mohamad Hammoud",
                "favorite_tv_show": "the_wire"
            }
        }

#TODO add auth
@app.post("/", response_description="Add new user", response_model=UserModel)
async def create_user(user: UserModel = Body(...)):
    user = jsonable_encoder(user)
    new_user = await db["users"].insert_one(user)
    created_user = await db["users"].find_one({"_id": new_user.inserted_id})
    return JSONResponse(status_code=status.HTTP_201_CREATED, content=created_user)


@app.get(
    "/", response_description="List users", response_model=List[UserModel]
)
async def list_users():
    users = await db["users"].find().to_list(1000)
    return users


#TODO add auth
@app.put("/{id}", response_description="Update a user", response_model=UserModel)
async def update_user(id: str, user: UpdateUserModel = Body(...)):
    user = {k: v for k, v in user.dict().items() if v is not None}

    if len(user) >= 1:
        update_result = await db["users"].update_one({"_id": id}, {"$set": user})

        if update_result.modified_count == 1:
            if (
                updated_user := await db["users"].find_one({"_id": id})
            ) is not None:
                return updated_user

    if (existing_user := await db["users"].find_one({"_id": id})) is not None:
        return existing_user

    raise HTTPException(status_code=404, detail=f"User {id} not found")


#TODO add auth
@app.delete("/{id}", response_description="Delete a user")
async def delete_user(id: str):
    delete_result = await db["users"].delete_one({"_id": id})

    if delete_result.deleted_count == 1:
        return Response(status_code=status.HTTP_204_NO_CONTENT)

    raise HTTPException(status_code=404, detail=f"user {id} not found")


@app.get("/{tvShow}", response_description="Get users by favorite TV show", response_model=List[UserModel])
async def get_users_by_show(tvShow: TVShow):
    users = await db["users"].find({"favorite_tv_show":tvShow}).to_list(1000)
    return users
